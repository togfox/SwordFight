battlefield = {}

local function drawBattleSpace()


    local drawx = 300
    local drawy = 350


    local player1square = fun.getPlayerSquare(1)
    local player2square = fun.getPlayerSquare(2)
    local stance1 = fun.getPlayerStance(1)
    local stance2 = fun.getPlayerStance(2)

    for i = 1, 5 do
        love.graphics.setColor(1,1,1,1)
        love.graphics.rectangle("line", drawx + (i * 200), drawy, 200, 300)
        if i == player1square then
            -- draw player 1
            local playercard = fun.getPlayerCard(1)
            if playercard.isDamaged then
                love.graphics.setColor(0,0,1,0.25)
            else
                love.graphics.setColor(0,0,1,1)
            end
            if stance1 == enum.attackStanceHeaven then
                love.graphics.rectangle("fill", drawx + (i * 200), drawy, 100, 150)
            else
                love.graphics.rectangle("fill", drawx + (i * 200), drawy + 150, 100, 150)
            end
        end
        if i == player2square then
            -- draw player 2
            local playercard = fun.getPlayerCard(2)
            if playercard.isDamaged then
                love.graphics.setColor(1,0,0,0.25)
            else
                love.graphics.setColor(1,0,0,1)
            end
            if stance2 == enum.attackStanceHeaven then
                love.graphics.rectangle("fill", drawx + (i * 200) + 100, drawy, 100, 150)
            else
                love.graphics.rectangle("fill", drawx + (i * 200) + 100, drawy + 150, 100, 150)
            end
        end
    end
    love.graphics.setColor(1,1,1,1)
end

local function drawHand(playernum)

    for i = 1, 2 do
        if playernum == 1 and i == 1 then
            drawx = 150
        elseif playernum == 1 and i == 2 then
            drawx = 350
        elseif playernum == 2 and i == 1 then
            drawx = 1550
        elseif playernum == 2 and i == 2 then
            drawx = 1750
        end
        drawy = 450

        card = {}
        card = HAND[playernum][i]
        if card ~= nil then
            love.graphics.print("Changes stance: " .. tostring(card.changesStance), drawx, drawy)
            love.graphics.print("Attack stance: " .. tostring(card.attackStance), drawx, drawy + 20)
            love.graphics.print("Min range: " .. tostring(card.minRange), drawx, drawy + 40)
            love.graphics.print("Max range: " .. tostring(card.maxRange), drawx, drawy + 60)
            love.graphics.print("Strike type: " .. tostring(card.strikeType), drawx, drawy + 80)
            love.graphics.print("Movement: " .. tostring(card.movement), drawx, drawy + 100)
            love.graphics.print("Counter attack: " .. tostring(card.isCounterAttack), drawx, drawy + 120)
        end

    end
end

function battlefield.draw()

    love.graphics.setColor(1,1,1,1)
    -- drawHand(1)
    -- drawHand(2)

    drawBattleSpace()
end

function battlefield.addButtons()
    -- battle button. A normal button
    local mybutton = buttons.create(1750, 975, enum.buttonBattle)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 150)
    buttons.setHeight(mybutton, 50)
    buttons.setDrawOutline(mybutton, true)
    buttons.setBGColour(mybutton, {0,0,0,0})
    buttons.setLabel(mybutton, "BATTLE!")
    buttons.setLabelColour(mybutton, {1,1,1,1})
    buttons.setScene(mybutton, enum.sceneBattle)

    -- random card
    local mybutton = buttons.create(1550, 150, enum.buttonRandomCard1)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 150)
    buttons.setHeight(mybutton, 50)
    buttons.setDrawOutline(mybutton, true)
    buttons.setBGColour(mybutton, {0,0,0,0})
    buttons.setLabel(mybutton, "Random card")
    buttons.setLabelColour(mybutton, {1,1,1,1})
    buttons.setScene(mybutton, enum.sceneBattle)

    local mybutton = buttons.create(1550, 850, enum.buttonRandomCard2)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 150)
    buttons.setHeight(mybutton, 50)
    buttons.setDrawOutline(mybutton, true)
    buttons.setBGColour(mybutton, {0,0,0,0})
    buttons.setLabel(mybutton, "Random card")
    buttons.setLabelColour(mybutton, {1,1,1,1})
    buttons.setScene(mybutton, enum.sceneBattle)



    local nextbuttonid = #GUI_BUTTONS + 10
    local nextcardslot = 100            -- used to allocate the next X value

    for k, card in pairs(PLAYER_DECK[1]) do
        if card.x == nil then
            card.x = nextcardslot
            nextcardslot = nextcardslot + 150
        end
        local mybutton = buttons.create(card.x - 5, 95, nextbuttonid)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
        buttons.setWidth(mybutton, CARD_WIDTH)
        buttons.setHeight(mybutton, 165)
        buttons.setDrawOutline(mybutton, true)
        buttons.setBGColour(mybutton, {0,0,0,0})
        buttons.setLabelColour(mybutton, {1,1,1,1})
        buttons.setLabel(mybutton, fun.determineButtonLabel(card))
        buttons.setScene(mybutton, enum.sceneBattle)

        mybutton.card = card
        nextbuttonid = nextbuttonid + 1
    end

    local nextcardslot = 100            -- used to allocate the next X value
    for k, card in pairs(PLAYER_DECK[2]) do
        if card.x == nil then
            card.x = nextcardslot
            nextcardslot = nextcardslot + 150
        end
        local mybutton = buttons.create(card.x - 5, 795, nextbuttonid)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
        buttons.setWidth(mybutton, CARD_WIDTH)
        buttons.setHeight(mybutton, 165)
        buttons.setDrawOutline(mybutton, true)
        buttons.setBGColour(mybutton, {0,0,0,0})
        buttons.setLabel(mybutton, fun.determineButtonLabel(card))
        buttons.setLabelColour(mybutton, {1,1,1,1})
        buttons.setScene(mybutton, enum.sceneBattle)

        mybutton.card = card
        nextbuttonid = nextbuttonid + 1
    end
end




return battlefield
