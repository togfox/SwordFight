constants = {}

function constants.load()

    GAME_VERSION = "0.01"

    SCREEN_STACK = {}

    SCREEN_WIDTH, SCREEN_HEIGHT = res.get_game_size()
    print("Screen width/height = " .. SCREEN_WIDTH, SCREEN_HEIGHT)

    -- camera
    ZOOMFACTOR = 1.0
    TRANSLATEX = cf.round(SCREEN_WIDTH / 2)		-- starts the camera in the middle of the ocean
    TRANSLATEY = cf.round(SCREEN_HEIGHT / 2)	-- need to round because this is working with pixels

    cam = nil       -- camera
    AUDIO = {}
    MUSIC_TOGGLE = true     --! will need to build these features later
    SOUND_TOGGLE = true

    IMAGE = {}
    FONT = {}

    DEBUG = true

    -- set the folders based on fused or not fused
    savedir = love.filesystem.getSourceBaseDirectory()
    if love.filesystem.isFused() then
        savedir = savedir .. "\\savedata\\"
    else
        savedir = savedir .. "/FormulaSpeed/savedata/"
    end

    enums.load()
    -- add extra items below this line

    PLAYER_DECK = {}
    HAND = {}
    HAND[1] = {}
    HAND[2] = {}

    CARD_WIDTH = 145

end

return constants
