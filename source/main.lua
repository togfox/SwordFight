inspect = require 'lib.inspect'
-- https://github.com/kikito/inspect.lua

res = require 'lib.resolution_solution'
-- https://github.com/Vovkiv/resolution_solution

Camera = require 'lib.cam11.cam11'
-- https://notabug.org/pgimeno/cam11

bitser = require 'lib.bitser'
-- https://github.com/gvx/bitser

nativefs = require 'lib.nativefs'
-- https://github.com/EngineerSmith/nativefs

lovelyToasts = require 'lib.lovelyToasts'
-- https://codeberg.org/togfox/Lovely-Toasts

-- require 'lib.lovedebug'
-- https://github.com/flamendless/lovedebug

o_ten_one = require "o-ten-one"

-- these are core modules
require 'lib.buttons'
require 'enums'
require 'constants'
fun = require 'functions'
cf = require 'lib.commonfunctions'

require 'battlefield'

function love.resize(w, h)
	res.resize(w, h)
end

function love.keyreleased( key, scancode )
	if key == "escape" then
		cf.removeScreen(SCREEN_STACK)
	end
end

function love.wheelmoved(x, y)
    if y > 0 then
        -- wheel moved up. Zoom in
        ZOOMFACTOR = ZOOMFACTOR + 0.05
        -- if cf.round(ZOOMFACTOR,2) == 0.60 then ZOOMFACTOR = 0.65 end
    end
    if y < 0 then
        ZOOMFACTOR = ZOOMFACTOR - 0.05
        -- if cf.round(ZOOMFACTOR,2) == 0.60 then ZOOMFACTOR = 0.55 end
    end

    if ZOOMFACTOR < 0.05 then ZOOMFACTOR = 0.05 end
    if ZOOMFACTOR > 3 then ZOOMFACTOR = 3 end
    -- print("Zoom factor = " .. ZOOMFACTOR)
end

function love.mousereleased(x, y, button, isTouch)
	local rx, ry = res.to_game(x, y)
	-- MOUSEX, MOUSEY = res.to_window(rx, ry)							-- I think this is old code that can be deleted
	lovelyToasts.mousereleased(rx, ry, button)

	-- detect which card is clicked
	if button == 1 then
		-- see if the brake button is pressed
		local clickedButtonID = buttons.getButtonID(rx, ry)
		if clickedButtonID ~= nil then print("Button clicked: " .. clickedButtonID) end

		if clickedButtonID == enum.buttonBattle then
			if #HAND[1] == 2 and #HAND[2] == 2 then
				fun.battle()
				HAND[1] = {}
				HAND[2] = {}
				GUI_BUTTONS = {}
				battlefield.addButtons()
			end
		elseif clickedButtonID == enum.buttonRandomCard1 then
			if #HAND[1] < 2 then
				local thiscard = PLAYER_DECK[1][love.math.random(2, #PLAYER_DECK[1])]
				table.insert(HAND[1], thiscard)
				fun.addCardToHand(1, thiscard)
			end
		elseif clickedButtonID == enum.buttonRandomCard2 then
			if #HAND[2] < 2 then
				local thiscard = PLAYER_DECK[2][love.math.random(2, #PLAYER_DECK[2])]
				table.insert(HAND[2], thiscard)
				fun.addCardToHand(2, thiscard)
			end

		elseif clickedButtonID ~= nil then
			-- do something
			local thisbutton = buttons.getButtonObject(clickedButtonID)
			local thiscard = thisbutton.card

			if thiscard ~= nil then
				if thiscard.owner == 1 and #HAND[1] < 2 then
					table.insert(HAND[1], thiscard)
					fun.addCardToHand(1, thiscard)
				end
				if thiscard.owner == 2 and #HAND[2] < 2 then
					table.insert(HAND[2], thiscard)
					fun.addCardToHand(2, thiscard)
				end
			end
		end

	end

end

function love.load()

	res.conf({game_width = 1920, game_height = 1080, scale_mode = 1})
	res.setMode(0, 0, {resizable=true, display=1})

	constants.load()		-- also loads enums
	fun.loadFonts()
    fun.loadAudio()
	fun.loadImages()

	cam = Camera.new(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 1)
	cam:setZoom(ZOOMFACTOR)
	cam:setPos(TRANSLATEX,	TRANSLATEY)

	love.window.setTitle("Sword fight " .. GAME_VERSION)

	love.keyboard.setKeyRepeat(true)

	if not DEBUG then
		cf.addScreen(enum.sceneSplash, SCREEN_STACK)
		splash = o_ten_one()
		splash.onDone = function()
			cf.swapScreen(enum.sceneBattle, SCREEN_STACK)
		end
	else
		cf.addScreen(enum.sceneBattle, SCREEN_STACK)
	end

	lovelyToasts.canvasSize = {SCREEN_WIDTH, SCREEN_HEIGHT}
	lovelyToasts.options.tapToDismiss = true
	lovelyToasts.options.queueEnabled = true
	-- lovelyToasts.show("This is a Lövely toast :)")
	-- =============================================

	SPECIAL_ATTACK_DECK = {}
	SPECIAL_ATTACK_DECK = fun.createSpecialAttackDeck()

	PLAYER_DECK = {}
	PLAYER_DECK[1] = fun.createNormalDeck(1)
	PLAYER_DECK[2] = fun.createNormalDeck(2)

	-- add special card to player deck 1
	local rndnum = love.math.random(1, #SPECIAL_ATTACK_DECK)
	local thiscard = SPECIAL_ATTACK_DECK[rndnum]
	thiscard.owner = 1
	table.insert(PLAYER_DECK[1], thiscard )
	table.remove(SPECIAL_ATTACK_DECK, rndnum)

	-- -- add special card to player deck 2
	local rndnum = love.math.random(1, #SPECIAL_ATTACK_DECK)
	thiscard = SPECIAL_ATTACK_DECK[rndnum]
	thiscard.owner = 2
	table.insert(PLAYER_DECK[2], thiscard)
	table.remove(SPECIAL_ATTACK_DECK, rndnum)

	-- print(inspect(PLAYER_DECK[1]))
	-- print("******************")
	-- print(inspect(PLAYER_DECK[2]))


	battlefield.addButtons()

end


function love.draw()
    res.push()
	cam:attach()

	local currentscreen = cf.getCurrentScreenName()
	if currentscreen == enum.sceneSplash then
		splash:draw()
	else
		battlefield.draw()
	end

	cam:detach()
	buttons.drawButtons()
	lovelyToasts.draw()
	res.pop()
end


function love.update(dt)

	local currentscreen = cf.getCurrentScreenName()

	if currentscreen == enum.sceneSplash then
		cf.playAudio(enum.audioSplashScreen, false, true)
		splash:update(dt)
	end


	-- cam:setZoom(ZOOMFACTOR)
    -- cam:setPos(TRANSLATEX, TRANSLATEY)
	lovelyToasts.update(dt)

end
