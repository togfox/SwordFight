enums = {}

function enums.load()
    enum = {}

    enum.sceneSplash = 1
    enum.sceneMainMenu = 2
    enum.sceneCredits = 3
    enum.sceneBattle = 4

    enum.buttonMainMenuExitGame = 1
    enum.buttonMainMenuContinue = 2
    enum.buttonBattle = 3
    enum.buttonRandomCard1 = 4
    enum.buttonRandomCard2 = 5

    enum.fontDefault = 1
    enum.fontMedium = 2
    enum.fontLarge = 3
    enum.fontCorporate = 4
    enum.fontalienEncounters48 = 5

    enum.audioSplashScreen = 1
    enum.audioMainMenu = 2
    -- add extra items below this line

    enum.cardTypePlayable = 1
    enum.cardTypePlayer = 2
    enum.cardTypeBattlefield = 3

    enum.attackStanceEarth = 1
    enum.attackStanceHeaven = 2
    enum.attackStanceBoth = 3

    enum.strikeTypeUp = 1
    enum.strikeTypeDown = 2
    enum.strikeTypeMid = 3
    enum.strikeTypeUpDown = 4


end

return enums
