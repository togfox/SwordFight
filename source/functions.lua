functions = {}

function functions.loadImages()
    -- IMAGE[enum.imageXXX] = love.graphics.newImage("assets/images/XXX.png")
end

function functions.loadFonts()
	-- love.graphics.setFont(FONT[enum.fontDefault])

    FONT[enum.fontDefault] = love.graphics.newFont("assets/fonts/Vera.ttf", 12)
    FONT[enum.fontMedium] = love.graphics.newFont("assets/fonts/Vera.ttf", 14)
    FONT[enum.fontLarge] = love.graphics.newFont("assets/fonts/Vera.ttf", 18)
    FONT[enum.fontCorporate] = love.graphics.newFont("assets/fonts/CorporateGothicNbpRegular-YJJ2.ttf", 36)
    FONT[enum.fontalienEncounters48] = love.graphics.newFont("assets/fonts/aliee13.ttf", 48)

    love.graphics.setFont(FONT[enum.fontDefault])
end

function functions.loadAudio()
    AUDIO[enum.audioSplashScreen] = love.audio.newSource("assets/audio/PowerUp1.mp3", "static")
end

function functions.createSpecialAttackDeck()
    local deck = {}

    -- special attack
    local thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.changesStance = true
    thiscard.attackStance  = enum.attackStanceEarth
    thiscard.minRange = 2
    thiscard.maxRange = 3
    thiscard.strikeType = enum.strikeTypeUpDown
    table.insert(deck, thiscard)

    local thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.changesStance = true
    thiscard.attackStance  = enum.attackStanceHeaven
    thiscard.minRange = 0
    thiscard.maxRange = 1
    thiscard.strikeType = enum.strikeTypeDown
    table.insert(deck, thiscard)

    -- counter attack
    local thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.attackStance = enum.attackStanceBoth
    thiscard.isCounterAttack = true
    table.insert(deck, thiscard)

    return deck
end

function functions.createNormalDeck(playerno)
    -- a normal deck is the deck with normal player cards. Every player has an identical normal deck
    -- does not include special cards
    local deck = {}

    -- player
    local thiscard = {}
    thiscard.cardType = enum.cardTypePlayer
    thiscard.owner = playerno
    thiscard.isDamaged = false
    thiscard.stance = enum.attackStanceHeaven
    if playerno == 1 then
        thiscard.square = 1
    else
        thiscard.square = 5
    end
    table.insert(deck, thiscard)

    -- movement
    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.movement = 1
    table.insert(deck, thiscard)

    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.movement = -1
    table.insert(deck, thiscard)

    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.movement = 2
    table.insert(deck, thiscard)

    -- change stance
    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.changesStance = true
    table.insert(deck, thiscard)

    -- attack
    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.attackStance  = enum.attackStanceEarth
    thiscard.minRange = 1
    thiscard.maxRange = 1
    thiscard.strikeType = enum.strikeTypeUp
    table.insert(deck, thiscard)

    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.attackStance  = enum.attackStanceHeaven
    thiscard.minRange = 2
    thiscard.maxRange = 2
    thiscard.strikeType = enum.strikeTypeDown
    table.insert(deck, thiscard)

    thiscard = {}
    thiscard.cardType = enum.cardTypePlayable
    thiscard.owner = playerno
    thiscard.attackStance  = enum.attackStanceBoth
    thiscard.minRange = 0
    thiscard.maxRange = 0
    thiscard.strikeType = enum.strikeTypeMid
    table.insert(deck, thiscard)

    return deck
end

function functions.getPlayerSquare(playerno)
    for j, deck in pairs(PLAYER_DECK) do
        for k, card in pairs(deck) do
            if card.cardType == enum.cardTypePlayer and card.owner == playerno then
                return card.square
            end
        end
    end
end

function functions.getPlayerStance(playerno)
    for j, deck in pairs(PLAYER_DECK) do
        for k, card in pairs(deck) do
            if card.owner == playerno and card.cardType == enum.cardTypePlayer then
                return card.stance
            end
        end
    end
    error()     -- should never happen
end

function functions.getPlayerCard(playerno)

    -- there are two decks one for each player [1], [2]
    for j, deck in pairs(PLAYER_DECK) do
        for k, card in pairs(deck) do
            if card.owner == playerno and card.cardType == enum.cardTypePlayer then
                return card
            end
        end
    end
    print("Error. Player no:")
    print(playerno)
    error()     -- should never happen
end

function functions.determineButtonLabel(card)
    local txt = ""

    if card.cardType == enum.cardTypePlayer then
        txt = txt  .. "Damaged: " .. tostring(card.isDamaged) .. "\n"
        txt = txt  .. "Stance: " .. tostring(card.stance) .. "\n"
        txt = txt  .. "Square: " .. tostring(card.square) .. "\n"
    elseif card.cardType == enum.cardTypePlayable then
        txt = txt  .. "Changes stance: " .. tostring(card.changesStance) .. "\n"
        txt = txt  .. "Attack stance: " .. tostring(card.attackStance) .. "\n"
        txt = txt  .. "Min range: " .. tostring(card.minRange) .. "\n"
        txt = txt  .. "Max range: " .. tostring(card.maxRange) .. "\n"
        txt = txt  .. "Strike type: " .. tostring(card.strikeType) .. "\n"
        txt = txt  .. "Movement: " .. tostring(card.movement) .. "\n"
        txt = txt  .. "Counter attack: " .. tostring(card.isCounterAttack) .. "\n"
    end
    return txt
end

function functions.addCardToHand(playerno, card)
    -- adds the card to either player hand one or two
    -- creates a button on the fly
    -- assumes the card is already added - this simply adds the button to the global collection of buttons
    local nextbuttonid, drawx, drawy
    if playerno == 1 then
        nextbuttonid = #GUI_BUTTONS + 1
        if #HAND[1] == 1 then drawx = 100 end
        if #HAND[1] == 2 then drawx = 300 end
    else    -- playeno == 2
        nextbuttonid = #GUI_BUTTONS + 1
        if #HAND[2] == 1 then drawx = 1550 end
        if #HAND[2] == 2 then drawx = 1750 end
    end

    drawy = 350

    if drawx == nil then
        print(nextbuttonid, drawx, drawy, #HAND[1], #HAND[2])
    end

    local mybutton = buttons.create(drawx, drawy, nextbuttonid)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, CARD_WIDTH)
    buttons.setHeight(mybutton, 165)
    buttons.setDrawOutline(mybutton, true)
    buttons.setBGColour(mybutton, {0,0,0,0})
    buttons.setLabel(mybutton, fun.determineButtonLabel(card))
    buttons.setLabelColour(mybutton, {1,1,1,1})
    buttons.setScene(mybutton, enum.sceneBattle)

    mybutton.card = card

    print("Added button id: " .. nextbuttonid)
end

function functions.battle()

    local thisdeck = {}

    HAND[1][1].actionnumber = 1
    HAND[1][2].actionnumber = 2
    HAND[2][1].actionnumber = 1
    HAND[2][2].actionnumber = 2

    table.insert(thisdeck, HAND[1][1])
    table.insert(thisdeck, HAND[2][1])
    table.insert(thisdeck, HAND[1][2])
    table.insert(thisdeck, HAND[2][2])

    -- order of execution
    -- 1) heaven stance charge (2 movement)
    -- 2) heaven stance approach/retreat (1 movement)
    -- 3) heaven stance change stance

    -- 4) earth stance charge
    -- 5) earth stance approach/retreat
    -- 6) earth stance change stance
    -- 7) attack/special

    -- determine priority
    for k, card in pairs(thisdeck) do

        if card.owner == nil then
            print(inspect(card))
            error()
        end

        local playercard = fun.getPlayerCard(card.owner)
        local playerstance = playercard.stance

        card.sequence = love.math.random(1, 1000) / 1000            -- random number that avoids sequences being exactly the same
        if card.actionnumber == 2 then card.sequence = card.sequence + 10 end                       -- action #2 goes to the bottom
        if playerstance == enum.attackStanceEarth then card.sequence = card.sequence + 3 end        -- earth goes to the bottom
        if card.strikeType ~= nil then card.sequence = card.sequence + 7 end          -- attack is pushed down
        if card.movement == 2 then card.sequence = card.sequence + 1 end
        if card.movement == 1 or card.movement == -1 then card.sequence = card.sequence + 2 end
        if card.changesStance then card.sequence = card.sequence + 3 end
    end

    -- print(inspect(thisdeck))
    table.sort(thisdeck, function (a,b)
        return (a.sequence < b.sequence)                --! this needs to use frating and brating
    end)
    -- print(inspect(thisdeck))

    -- proces cards in order noting some things will be simultaneous
    local lastsequence = 0
    local player1strikes = false
    local player2strikes = false

    for i = 1, 4 do
        local card = thisdeck[i]
        local playercard = fun.getPlayerCard(card.owner)
        local otherplayercard
        if playercard.owner == 1 then
            otherplayercard = fun.getPlayerCard(2)
        else
            otherplayercard = fun.getPlayerCard(1)
        end

        if card.movement ~= nil then
            if card.owner == 1 then
                playercard.square = playercard.square + card.movement
            else
                playercard.square = playercard.square - card.movement
            end
            print("Player " .. card.owner .. " moves")

            playercard.square = cf.clamp(playercard.square, 1, 5)
        end
        if card.strikeType ~= nil then
            local distance = otherplayercard.square - playercard.square
            if distance >= card.minRange and distance <= card.maxRange then
                if card.owner == 1 then
                    player1strikes = true
                    print("Player 1 strikes!")
                else
                    player2strikes = true
                    print("Player 2 strikes!")
                end
                otherplayercard.isDamaged = true
            else
                --! swing and miss
                print("Player " .. card.owner .. " strikes at nothing")
            end
        end
        if card.changesStance then
            print("Player changed stances: " .. card.owner)
            if playercard.stance == enum.attackStanceEarth then
                playercard.stance = enum.attackStanceHeaven
            else
                playercard.stance = enum.attackStanceEarth
            end
        end
    end


end

return functions
